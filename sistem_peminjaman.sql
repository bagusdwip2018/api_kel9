USE sistem_peminjaman;
CREATE TABLE `mahasiswa` (
  `mhsNpm` CHAR(12) NOT NULL,
  `mhsNama` VARCHAR(255) DEFAULT NULL,
  `mhsAlamat` TEXT DEFAULT NULL,
  `mhsFakultas` VARCHAR(255) DEFAULT NULL,
  `mhsProdi` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`mhsNpm`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;
INSERT  INTO `mahasiswa`(`mhsNpm`,`mhsNama`,`mhsAlamat`,`mhsFakultas`,`mhsProdi`) VALUES ('11312136','Mahasiswa1','alamat mahasiswa 1\r\n','Teknik','Teknik Informatika'),('11312137','Mahasiswa2','alamat mahasiswa 2\r\n','Teknik','Teknik Elektro'),('11312138','Mahasiswa 3','alamat mahasiswa 3','fakultas mahasiswa 3','prodi mahasiswa 3');

CREATE TABLE `user` (
  `id_user` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) DEFAULT NULL,
  `username` VARCHAR(50) DEFAULT NULL,
  `pass` VARCHAR(255) DEFAULT NULL,
  `role` VARCHAR(20) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
INSERT  INTO `user`(`id_user`,`name`,`username`,`pass`,`role`) VALUES (1,'admin','tes','28b662d883b6d76fd96e4ddc5e9ba780','Admin'),(2,'admin2','Admin2','7a8a80e50f6ff558f552079cefe2715d','Admin'),(4,'tess1','tes11','22daf1a39b6e5ea16554f59e472d96f6','Admin');
SELECT * FROM `user`;